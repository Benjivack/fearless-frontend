function createCard(name, description, pictureUrl, starts, ends, locations, options) {
    options = {
        month: 'numeric',
        day: 'numeric',
        year: 'numeric'
    }
    const startDate = new Date(starts).toLocaleDateString('en-US', options)
    const endDate = new Date(ends).toLocaleDateString('en-US', options)
    return `
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class='card-subtitle mb-2 text-muted'>${locations}</h6>
          <p class="card-text">${description}</p>
          </div>
        </div>
        <div class='card-footer'>${startDate} - ${endDate}</div>
      </div>
    `;
  }
window.addEventListener('DOMContentLoaded', async () => {
const url ='http://localhost:8000/api/conferences/';
try{
    const response = await fetch(url);
    if(!response.ok) {
        // figure out what to do when response is bad
        const errorMessage =`
        <div class ='alert 'alert-danger' role="alert">`;
        const errorContainer = document.createElement('div');
        errorContainer.innerHTML = errorMessage;
        const firstChild = document.body.firstChild;
        document.body.insertBefore(errorContainer, firstChild)
    } else {
        const data = await response.json();
        let counter = 0 // keep track of which column we want our content in
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const locations =details.conference.location.name;
              const pictureUrl = details.conference.location.picture_url;
              const starts = details.conference.starts;
              const ends = details.conference.ends;
              const html = createCard(name, description, pictureUrl, starts, ends, locations);
              const columns = document.querySelectorAll('.col'); //column is a nodelist
              const column =columns[counter];//column is an object, grabs location from the quereySelector
              column.innerHTML += html;
              counter++;
              if (counter===3){
                counter = 0
            }
        }
    }
}} catch (error) {
    //figure out if error is raised
    console.error('error', error)
}
});
